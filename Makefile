##
## EPITECH PROJECT, 2020
## my_libs
## File description:
## Makefile

ECHO	=	/bin/echo -e
DEFAULT	=	"\e[0m"
BOLD_T	=	"\e[1m"
DIM_T	=	"\e[2m"
UNDLN_T	=	"\e[4m"
BLACK_C	=	"\e[30m"
RED_C	=	"\e[31m"
GREEN_C	=	"\e[32m"
YELLO_C	=	"\e[33m"
BLUE_C	=	"\e[34m"
MAGEN_C	=	"\e[35m"
CYAN_C	=	"\e[36m"
WHITE_C	=	"\e[97m"
DEFAULT_C	=	"\e[39m"
LIGHT_GREY	=	"\e[37m"
DARK_GREY	=	"\e[90m"
LIGHT_RED	=	"\e[91m"
LIGHT_GREEN	=	"\e[92m"
LIGHT_YELLO	=	"\e[93m"
LIGHT_BLUE	=	"\e[94m"
LIGHT_MAGEN	=	"\e[95m"
LIGHT_CYAN	=	"\e[96m"
LINE_RETURN	=	$(ECHO) ""

SRC =	./src/main.c

OBJ =	$(SRC:.c=.o)

NAME =	project
CFLAGS = -Wall -Wextra -I./include/

all :	message $(NAME) done_print

$(NAME) :	$(OBJ)
	# make -C lib/my
	# make -C lib/printlib
	# make -C lib/linkedlib
	gcc -o $(NAME) $(OBJ) -L./lib/ -lmy -lprint -llink

clean :
	rm -f $(OBJ)
	# make clean -C lib/my
	# make clean -C lib/printlib
	# make clean -C lib/linkedlib
	rm -f ~* \#*\#

fclean :	clean
	rm -f $(NAME)
	make fclean -C lib/my
	make fclean -C lib/printlib
	make fclean -C lib/linkedlib

re :	fclean all

debug: CFLAGS += -g
debug: re debug_mode

message:
	@$(LINE_RETURN)
	@$(LINE_RETURN)
	@$(ECHO) $(BOLD_T)$(GREEN_C)"██████╗ ██████╗  ██████╗      ██╗███████╗ ██████╗████████╗"$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)"██╔══██╗██╔══██╗██╔═══██╗     ██║██╔════╝██╔════╝╚══██╔══╝"$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)"██████╔╝██████╔╝██║   ██║     ██║█████╗  ██║        ██║   "$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)"██╔═══╝ ██╔══██╗██║   ██║██   ██║██╔══╝  ██║        ██║   "$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)"██║     ██║  ██║╚██████╔╝╚█████╔╝███████╗╚██████╗   ██║   "$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)"╚═╝     ╚═╝  ╚═╝ ╚═════╝  ╚════╝ ╚══════╝ ╚═════╝   ╚═╝   "$(DEFAULT)
	@$(LINE_RETURN)

debug_mode:
	@$(ECHO) $(BOLD_T)$(GREEN_C)"  _____       _                 "$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)" |  __ \     | |                "$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)" | |  | | ___| |__  _   _  __ _ "$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)" | |  | |/ _ \ '_ \| | | |/ _\` |"$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)" | |__| |  __/ |_) | |_| | (_| |"$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)" |_____/ \___|_.__/ \__,_|\__, |"$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)"                           __/ |"$(DEFAULT)
	@$(ECHO) $(BOLD_T)$(GREEN_C)"                          |___/ "$(DEFAULT)
	@$(LINE_RETURN)

done_print:
	@$(LINE_RETURN)
	@$(ECHO) $(LIGHT_GREEN)"BUILD $(NAME) COMPLETE!"$(DEFAULT)
	@$(ECHO) $(LIGHT_GREEN)"-----------------------"$(DEFAULT)
	@$(LINE_RETURN)
