# my_libs

This project is my lib create during my first Epitech years.
For the moment I have 2 libs, one for all my printing functions, and the second is my lib create during the CPool.
__(Of course, full respect to the epitech's coding style)__

### printlib :
- `my_printf()` : This functions are the main function of this lib, work like the printf in elementary C functions.
- `my_strlen()` : Return the size of the string given in parameters.
- `print_helper()` : Print a helper (free point in all epitech's project).

### mylib :
These function are useless for me, but in some project, can be usefull.