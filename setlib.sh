#!/bin/sh

mkdir lib/ include/ src/
touch src/main.c
if [ $1 = "-a" ]
then
    mv my_lib/my/*.h include/
    mv my_lib/my/ lib/
    mv my_lib/printlib/*.h include/
    mv my_lib/printlib/ lib/
    mv my_lib/linkedlib/*.h include/
    mv my_lib/linkedlib/ lib/
    mv my_lib/Makefile ./
    rm -rf my_lib/
elif [ $1 = "-p" ]
then
    mv my_lib/printlib/*.h include/
    mv my_lib/printlib/ lib/
    mv my_lib/Makefile ./
    rm -rf my_lib/
elif [ $1 = "-m" ]
then
    mv my_lib/my/*.h include/
    mv my_lib/my/ lib/
    mv my_lib/Makefile ./
    rm -rf my_lib/
elif [ $1 = "-ll" ]
then
    mv my_lib/linkedlib/*.h include/
    mv my_lib/linkedlib/ lib/
    mv my_lib/Makefile ./
    rm -rf my_lib/
fi